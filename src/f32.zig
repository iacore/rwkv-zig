//! pure f32 model

/// how "wide" the model is
pub const N0 = 768;
pub const N_VOCAB = 50277;
pub const N_LAYER = 12;

// not sure what this is
pub const N1 = N0 * 4;

pub const Linear = struct {
    weight: *[N0]f32,
    bias: *[N0]f32,
};

/// time mix layer
pub const ATT = struct {
    time_decay: *[N0]f32,
    time_first: *[N0]f32,
    time_mix_k: *[N0]f32,
    time_mix_v: *[N0]f32,
    time_mix_r: *[N0]f32,
    key: *[N0][N0]f32,
    value: *[N0][N0]f32,
    receptance: *[N0][N0]f32,
    output: *[N0][N0]f32,
};

/// channel mix layer
pub const FFN = struct {
    time_mix_k: *[N0]f32,
    time_mix_r: *[N0]f32,
    key: *[N1][N0]f32,
    receptance: *[N0][N0]f32,
    value: *[N0][N1]f32,
};

/// RWKV block
pub const Mix = struct {
    ln1: Linear,
    att: ATT,
    ln2: Linear,
    ffn: FFN,
};

pub const RWKV = struct {
    emb: *[N_VOCAB][N0]f32,
    ln_in: Linear, // blocks.0.ln0
    layers: [N_LAYER]Mix,
    ln_out: Linear,
    head: *[N_VOCAB][N0]f32,
};
