//! quantized q4_1 model

pub const q4_1 = struct {
    d: f32,
    ints: @Vector(32, i4),
};

/// RWKV block
pub const Layer = struct {
    @"ln1.weight": *[768]f32,
    @"ln1.bias": *[768]f32,
    @"ln2.weight": *[768]f32,
    @"ln2.bias": *[768]f32,
    @"att.time_decay": *[768]f32,
    @"att.time_first": *[768]f32,
    @"att.time_mix_k": *[768]f32,
    @"att.time_mix_v": *[768]f32,
    @"att.time_mix_r": *[768]f32,
    @"att.key.weight": *[768][768]q4_1,
    @"att.value.weight": *[768][768]q4_1,
    @"att.receptance.weight": *[768][768]q4_1,
    @"att.output.weight": *[768][768]q4_1,
    @"ffn.time_mix_k": *[768]f32,
    @"ffn.time_mix_r": *[768]f32,
    @"ffn.key.weight": *[3072][768]q4_1,
    @"ffn.receptance.weight": *[768][768]q4_1,
    @"ffn.value.weight": *[768][3072]q4_1,
};

pub const Model = struct {
    // near in
    @"emb.weight": *[50277][768]q4_1,
    
    // ln for first layer
    @"ln_in.weight": *[768]f32, // blocks.0.ln0.weight
    @"ln_in.bias": *[768]f32, // blocks.0.ln0.bias

    layers: [12]Layer,

    // near out
    @"ln_out.weight": *[768]f32,
    @"ln_out.bias": *[768]f32,
    @"head.weight": *[50277][768]q4_1,
};
