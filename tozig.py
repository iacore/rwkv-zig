names = set()

for line in open("weights"):
    name, x, y, t = line.split("\t")
    t = t.strip().lower()
    if y == "-1":
        print(f"""@"{name}": [{x}]{t},""")
    else:
        print(f"""@"{name}": [{y}][{x}]{t},""")
