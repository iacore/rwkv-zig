names = set()

for line in open("weights"):
    name = line.split("\t")[0]
    names.add(name)

for i in range(12):
    names.remove(f"blocks.{i}.ln1.weight")
    names.remove(f"blocks.{i}.ln1.bias")
    names.remove(f"blocks.{i}.ln2.weight")
    names.remove(f"blocks.{i}.ln2.bias")
    names.remove(f"blocks.{i}.att.time_decay")
    names.remove(f"blocks.{i}.att.time_first")
    names.remove(f"blocks.{i}.att.time_mix_k")
    names.remove(f"blocks.{i}.att.time_mix_v")
    names.remove(f"blocks.{i}.att.time_mix_r")
    names.remove(f"blocks.{i}.att.key.weight")
    names.remove(f"blocks.{i}.att.value.weight")
    names.remove(f"blocks.{i}.att.receptance.weight")
    names.remove(f"blocks.{i}.att.output.weight")
    names.remove(f"blocks.{i}.ffn.time_mix_k")
    names.remove(f"blocks.{i}.ffn.time_mix_r")
    names.remove(f"blocks.{i}.ffn.key.weight")
    names.remove(f"blocks.{i}.ffn.receptance.weight")
    names.remove(f"blocks.{i}.ffn.value.weight")

for name in names:
    print(name)
